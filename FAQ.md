# Feddit FAQ:

## Allgemeines

### Wie finanziert sich die Instanz, kann man spenden?

Die Instanz wird privat betrieben und von wintermute privat finanziert.
Um geschäftliche Interessen auszuschließen, gibt es keine Spendenmöglichkeit.

### Wo findet sich das Impressum von feddit?

Ausschließlich privat genutzte Seiten, die keinen kommerziellen Hintergrund besitzen, fallen nicht unter die Impressumspflicht.
Daher gibt es keine Möglichkeit zu Spenden, auch werden Affiliate-Links und andere gewinnorientierte Werbung nicht geduldet.

[[1]](https://www.e-recht24.de/impressum/13095-impressum-fuer-die-private-homepage.html)
[[2]](https://www.bmuv.de/themen/verbraucherschutz-im-bmuv/digitaler-verbraucherschutz/impressumspflicht#c66866 )

### Werbung auf feddit

Werbung, insb. Affiliate-Links etc. sowie das Posten von Beiträgen in gewinnorientierter Absicht sind ausdrücklich nicht erwünscht.
Eine Diskussion/ der Austausch über Produkte sollte aber möglich sein.



### Was, wenn der Betreiber das Interesse verliert und den Laden einfach dicht macht?

Das wird so nicht passieren, in diesem Fall wird wintermute versuchen die Instanz inkl. Domain etc. abzutreten, an jemanden der gewillt ist das Ganze fortzuführen.

## Föderation

### Warum ist Instanz x/y auf der [Blocklist](https://feddit.de/instances)

Generell blockieren wir Instanzen

- die extremistische Inhalte verbreiten
- die in der Vergangenheit bereits negativ aufgefallen sind
- die NSFW-Content verbreiten (Porn,Adult,Gore,etc.)
- deren Sprachen wir nicht moderieren können
- deren Regeln nicht mit unseren Instanzregeln (TOS) kompatibel sind
- die keine manuelle Freischaltung von Accounts betreiben
- von Bots geflutet wurden
- zu erhöhtem Spam-Aufkommen beitragen

Die Instanzliste wird unregelmäßig überprüft, Anfragen zum Entfernen einer Instanz von der Blocklist bitte an das Admin-Team.
Leider bietet Lemmy bis zur Version 0.18.2 keine Möglichkeit einen Grund für den Block zu hinterlegen.

## Bots

### Darf ich einen Bot auf feddit betreiben?

Das hängt von Art und Funktionsumfang des Bots ab, und sollte mit dem Admin-Team abgesprochen sein.

- Bots-Accounts müssen als solche gekennzeichnet sein
- Der Source-Code sollte frei zugänglich sein