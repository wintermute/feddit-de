Hier soll ein alternativer, unabhängiger und selbstverwalteter Raum zum freien Meinungsaustausch, jenseits der Kontrolle großer Tech-Unternehmen entstehen.

Netiquette wird vorausgesetzt.
Gepflegt wird ein respektvoller Umgang - **ohne Hass, Hetze, Diskriminierung**.

Diese Community befindet sich im Aufbau und lebt von deiner Mitwirkung!

::: spoiler Regeln

Die folgenden Regeln sind eine (nicht vollständige) Liste von Verhaltensweisen, die nach Ermessen der Instanz-Admins und -Mods zur Löschung von Posts, Gruppen oder Sperrung von Konten führen können, wie in unseren Bedingungen beschrieben.

Bitte melde Verhalten, das dich stört den Admins/ Mods, und trage keine Konflikte in die Community.

Wir tolerieren kein diskriminierendes Verhalten und keine Inhalte, die die Unterdrückung von Mitgliedern marginalisierter Gruppen fördern oder befürworten. Diese Gruppen können durch eine der folgenden Eigenschaften gekennzeichnet sein (obwohl diese Liste natürlich unvollständig ist):

- ethnische Zugehörigkeit
- Geschlechtsidentität oder Ausdruck
- sexuelle Identität oder Ausdruck
- körperliche Merkmale oder Alter
- Behinderung oder Krankheit
- Nationalität, Wohnsitz, Staatsbürgerschaft
- Reichtum oder Bildung
- Religionszugehörigkeit, Agnostizismus oder Atheismus

Wir tolerieren kein bedrohliches Verhalten, Stalking und Doxxing.
Wir tolerieren keine Belästigungen, einschließlich Brigading, Dogpiling oder jede andere Form des Kontakts mit einem Benutzer, der erklärt hat, dass er nicht kontaktiert werden möchte.

- Sei respektvoll. Alle sind hier willkommen.
- Kein Rassismus, Sexismus, Ableismus, Homophobie, oder anderweitige Xenophobie
- Wir tolerieren kein Mobbing, einschließlich Beschimpfungen, absichtliches Misgendering oder Deadnaming.
- Wir dulden keine gewalttätige nationalistische Propaganda, Nazisymbolik oder die Förderung der Ideologie des Nationalsozialismus.
- Aktionen, die diese Instanz oder ihre Leistung beschädigen sollen, können zur sofortigen Sperrung des Kontos führen.
- Provokationen können nach Ermessen der Moderation entfernt werden
- Toxisches Verhalten wird nicht geduldet
- Keine Werbung
- Kein Spam
- Keine Pornografie / Adult Content
- In Deutschland illegale Inhalte werden gelöscht und können zur sofortigen Sperrung des Accounts führen.
  :::
  \
  Although this sites language is german,
  everyone is welcome to join, as we federate!

Server-Location: Nürnberg, Germany

::: spoiler Contact
e-mail: pgcn5lz86@relay.firefox.com\
matrix: [@winter:tilde.fun](https://matrix.to/#/@winter:tilde.fun)
:::

::: spoiler Attribution
This text was partly adapted and modified from [chaos.social ](https://chaos.social/about/more#rules).
It is free to be adapted and remixed under the terms of the [CC-BY (Attribution 4.0 International)](https://creativecommons.org/licenses/by/4.0/) license.
:::
