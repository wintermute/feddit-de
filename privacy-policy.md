# Privacy Policy

We provide this information according to the [EU Regulation 2016/679 (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679&from=EN) for those who consult the website https://feddit.de and request to register. Note that this information applies only to that website and not to other websites the user may consult through links.

## Data controller

The data controller is [@wintermute@feddit.de](https://feddit.de/u/wintermute) pgcn5lz86@relay.firefox.com

## What data is collected.

Regarding this point, we must distinguish two steps.

(a) Registration in the community: username, password, and email (optional);

(b) Access to the community and user activities: IP address, username, password, (email).

The user data (username, password, and email) are recorded in the database, and specifically, the passwords are “hashed” (i.e., transformed into alphanumeric strings using the hash function).

### Access to the community and user activities

Having completed the account creation process, the user can log in (Login) to the community through the browser, the IP address is acquired.

**Each user is responsible for the content they intend to post on the Lemmy community**.

In that phase, the personal data of users collected are the **username**, **password**, **email address**, and **IP address**.

### Who can access the data and for what activities.

The server administrator (instance) can read data of the activities performed on the community recorded on the server and precisely in the database or log files (username, email, IP address, community web address, type of activity - technically GET or POST).

The administrator only accesses users’ personal data for strictly technical reasons.

We should point out that access to users’ personal data, whether in the database or logs, is a specific activity that is not generally performed except to resolve particular conflicts or errors.

## The purposes of the processing.

The purpose is to consult this website or interact by posting content, comments, or creating other communities. Accessing this website, and requesting to register as a user, means the user gave consent.

Furthermore, the purposes are also related to server maintenance and system and application upgrades.

The optional, explicit, and voluntary sending of electronic mail to the addresses indicated on this site involves the acquisition of the sender’s address necessary for the replies and any other personal data contained in the message. These data are processed to respond to messages sent and handle related requests. Failure to provide personal data for communications with us or send requests will prevent evading them. We store data for the time strictly necessary for the purposes related to data processing.

## Legal basis for the processing

The processing of personal data is based on consent - according to article 6, par. 1, letter a) of EU Regulation 2016/679 - expressed by the user by browsing this website and its consultation, thus accepting this information.

Consent is optional, and the user can withdraw at any time by request sent by email to pgcn5lz86@relay.firefox.com, specifying that, in this case, whether the user does not consent, they cannot consult this website, either register or remain as a registered user.

Regarding server maintenance and system and application upgrades, the legal basis is the legitimate interest according to Article 6, letter f) of the EU Regulation 2016/679.


## Cookies

The only cookies are only **functional ones** and, therefore, no profiling or tracking activities.

**Thus, this site does not use cookies other than functional cookies solely for the functional purposes described above, and their installation does not require the user’s consent**.

## Data recipients

We do not communicate personal data collected from this website following its consultation to recipients or categories of recipients.

## Period for storing personal data

Apart from what is specified above, the data collected by this website during its operation are stored for the time strictly necessary for the activities specified. The data will be deleted or anonymized at the expiry date unless there are no other purposes for storing the same.

## Transferring personal data to a third country or international organization

The data controller, the administrator of Lemmy’s instance, does not transfer data outside the European Economic Area (EEA) if Lemmy is installed on the server located within the European Economic Area.

We feel it is appropriate to clarify this further.

Users registered on an instance are always solely responsible for their activities by creating communities or publishing posts or comments.

There is no transfer outside the SEE when registered users on an instance within the same EEA perform activities on the same server (instance). For example, our instance (https://feddit.de) is located in Germany and thus within the EEA. If users registered on our instance perform activities on our server, there is no data transfer outside the EEA. Similarly, there is no data transfer outside the EEA even if registered users on our instance subscribe, publish posts, or comments on other instances - for example - located outside the EEA. Indeed, in the latter case, our instance administrator can access the logs and see only the domain (and thus not even the full URL of the community on which activities are performed) and its IP address. No further user data is transferred outside the EEA by the administrator or automatically by the Lemmy platform. The user should be aware that their username in the form “@username@domainofcommunity” (e.g., in our case, @username@feddit.de) will be visible in the community in which they have intervened (e.g., to publish posts or comments).

There will be no transfer of data outside the EEA even if the user intends to create a community on the existing Lemmy instance within the same EEA.

All of this is because it is a proper function of the fediverse’s system and the ActivityPub protocol used by Lemmy.

## Security measures

Visitors’ or users’ data are processed lawfully and correctly by adopting appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of data. Your data in the communication session with this website are protected by a Secure Sockets Layer (SSL) certificate that uses a cryptographic presentation protocol, encrypting the information.

## Data subjects’ rights

Users (data subject) of this website may exercise the rights according to Articles 15 to 22 of EU Regulation 2016/679. You can lodge all requests to exercise these rights by writing to pgcn5lz86@relay.firefox.com

## Right to lodge a complaint

Whether a data subject considers that the processing of personal data relating to them as performed via this website infringes the Regulation, they have the right to lodge a complaint with the Garante according to Article 77 of the EU Regulation 2016/679.